
require('dotenv').config();

const app = require('./server');
require('./database');




app.listen(app.get('port'), () => {
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`);
});


/*
app.get('/anuncio', function (req, res) { var anuncios = []	var a1 = new Anuncio('Anuncio 1', 677)	anuncios.push(a1)	var a2 = new Anuncio('Anuncio 2', 56)	anuncios.push(a2)	res.send(JSON.stringify(anuncios)) })
app.post('/anuncio', function (req, res) {	
    MongoClient.connect(		
        'mongodb://localhost:27017/anuncios',		
        { useUnifiedTopology: true },		
        function (err, db) {			
            if (err) {				
                res.status(500)				
                res.json({					
                    mensaje: 'Error de la base de datos',					
                    insertado: false,				
                })			
            } else {				console.log('Conectado al servidor')				var a1 = new Anuncio(					req.body.descripcion,					parseFloat(req.body.precio)				)				var dbo = db.db('anuncios')				var collection = dbo.collection('anuncios')				collection.insertOne(a1, function (err, result) {					if (err) {						res.status(500)						res.json({							mensaje: 'Error al insertar el anuncio',							insertado: false,						})					} else {						res.status(201)						res.json({							insertado: true,						})					}					// Cerrar el cliente					db.close()				})			}		}	)})
app.get('/anuncio', function (req, res) {	MongoClient.connect(		'mongodb://localhost:27017/anuncios',		{ useUnifiedTopology: true },		function (err, db) {			if (err) {				res.status(500)				res.json({					mensaje: 'Error de la base de datos',					insertado: false,				})			} else {				console.log('Conectado al servidor')				var dbo = db.db('anuncios')				var collection = dbo.collection('anuncios')				collection.find().toArray(function (err, result) {					if (err) {						res.status(500)						res.json({							mensaje: 'Error al listar anuncios',							insertado: false,						})					} else {						res.status(200)						res.send(result)					}					// Cerrar el cliente					db.close()				})			}		}	)})
app.get('/anuncio/:id', function (req, res) {	var IDAnuncio = require('mongodb').ObjectID(req.params.id)	MongoClient.connect(		'mongodb://localhost:27017/anuncios',		{ useUnifiedTopology: true },		function (err, db) {			if (err) {				res.status(500)				res.json({					mensaje: 'Error de la base de datos',					insertado: false,				})			} else {				console.log('Conectado al servidor')				var dbo = db.db('anuncios')				var collection = dbo.collection('anuncios')				collection					.find({ _id: IDAnuncio })					.toArray(function (err, result) {						if (err) {							res.status(500)							res.json({								mensaje: 'Error al listar anuncios',								insertado: false,							})						} else {							res.status(200)							res.send(result[0])						}						// Cerrar el cliente						db.close()					})			}		}	)})
//Función DELETE /anuncio/:idapp.delete('/anuncio/:id', function (req, res) {	var IDAnuncio = require('mongodb').ObjectID(req.params.id)	MongoClient.connect(		'mongodb://localhost:27017/anuncios',		{ useUnifiedTopology: true },		function (err, db) {			if (err) {				res.status(500)				res.json({					mensaje: 'Error de la base de datos',					insertado: false,				})			} else {				console.log('Conectado al servidor')				var dbo = db.db('anuncios')				var collection = dbo.collection('anuncios')				collection.deleteOne({ _id: IDAnuncio }, function (					err,					result				) {					if (err) {						res.status(500)						res.json({							mensaje: 'Error al listar anuncios',							insertado: false,						})					} else {						res.status(200)						res.json({							mensaje: 'Borrado',						})					}					// Cerrar el cliente					db.close()				})			}		}	)})
*/
