const libroController = {};
const { restart } = require('nodemon');
const Book = require('../models/libro.model');


libroController.obtenerLibros = async (req, res) => {
    const librosArray = await Book.find();
    if(!librosArray.length) {
        res.status(200);
        res.json({
            mensaje : "No hay libros disponibles"
        });
    } else {
        res.status(200);
        res.json({
            mensaje : `Se obtuvieron ${librosArray.length} libros(s) de forma correcta`,
            detalle : (librosArray)
        });
    }
}

libroController.crearLibro = async (req, res) => {
    const { titulo, isbn } = req.body;
    const nuevoLibro = new Book({ titulo, isbn });
    await nuevoLibro.save()
        .then( respuesta => {
            res.status(201)
            res.json({
                mensaje : 'Libro creado',
                detalle : `ID: ${respuesta._id} - Título del libro: ${respuesta.titulo}`
            })
        })
        .catch(err => {
            if (err) {
                console.log("error => " + err)
                res.status(409)
                res.json({
                    mensaje : 'Error al guardar el libro',
                    detalle : err.message
                })
            }
        });
}

libroController.editarLibro = async (req, res) => {
    const { titulo, isbn } = req.body;
    Book.count({_id: req.body.id}, async function (err, count){ 
        if(count>0){
            await Book.findByIdAndUpdate( req.body.id, { titulo, isbn })
                .then( r => {
                    res.status(200);
                    res.json({
                        mensaje : "Libro editado con éxito",
                        detalle : r
                    })
                })
                .catch( err => {
                    if(err){ 
                        console.log("error => " + err)
                        res.status(409)
                        res.json({
                            mensaje : 'Error al editar el libro',
                            detalle : err
                        }) 
                    }
                })
        } else {
            res.status(404);
            res.json({
                mensaje: `No se encontró el id ${req.body.id}`
            });
        }
    }); 
}   


libroController.borrarLibro = async (req, res) => {
    await Book.findByIdAndDelete( req.body.id )
        .then( respuesta => {
            res.status(200)
            res.json({
                mensaje : 'Libro borrado',
                detalle : `Título del libro eliminado: ${respuesta.titulo}`
            })
        })
        .catch(err => {
            if (err) {
                console.log("error => " + err)
                res.status(409)
                res.json({
                    mensaje : 'Error al borrar el libro',
                    detalle : err
                })
            }
        });
}

module.exports = libroController;