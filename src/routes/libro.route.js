const  { Router } = require('express');
const router = Router();

const { obtenerLibros, crearLibro, editarLibro, borrarLibro } = require('../controllers/libro.controller');

router.get('/libro/', obtenerLibros);
router.post('/libro/', crearLibro);
router.patch('/libro/', editarLibro);
router.delete('/libro/', borrarLibro);

module.exports = router;