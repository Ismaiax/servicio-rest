// Creación del servidor
const express       = require('express');
const path          = require('path');
const bodyParser    = require('body-parser');

// Inicialización
const app = express();


// Configuración
app.set('port', process.env.PORT || 4000);


// Middleware
app.use(express.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));





// Rutas
app.use(require('./routes/index.route'));
app.use(require('./routes/libro.route'));



// Archivos estáticos
app.use(express.static(path.join(__dirname, 'public')));



module.exports = app;