const { Schema, model } = require('mongoose');

const BookSchema = new Schema({
    titulo: {
        type: String,
        required: true
    },
    isbn: {
        type: Number,
        required: true
    }
}, {
    timestamps : true
});

module.exports = model('Libro', BookSchema);